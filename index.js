

const getCube = 2 ** 3
console.log(`The cube of 2 is ${getCube}`)



const address = ["Sgt. De Leon", "Santolan", "Pasig"]

const [streetName, brgyName, cityName] = address

console.log(`I live at ${streetName} Street, Brgy. ${brgyName} ${cityName} City.`)



const animal =  {
	givenName: "Skippy",
	age: "9 years old",
	breed: "Shi-tzu"
}

const {givenName, age, breed} = animal

console.log(`${givenName} is our pet dog. She's now ${age} and she is a ${breed}`)



const numArray = [26, 78, 45, 2]

numArray.forEach((number) =>
	console.log(number));



class Dog {
	constructor (name, age, breed)
	{
		this.name = name,
		this.age = age,
		this.breed = breed
	}
}

const myDog = new Dog ()

myDog.name = "Skippy"
myDog.age = 9
myDog.breed = "Shi-tzu"

console.log(myDog)